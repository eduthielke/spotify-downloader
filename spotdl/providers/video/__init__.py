"""
Audio providers for spotdl.
"""

from spotdl.providers.video.youtube import YouTube
from spotdl.providers.video.ytmusic import YouTubeMusic
